<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

use Illuminate\Http\Request;
use App;

class WebsiteController extends Controller {

    public function index()
    {
        return view("pages.home");
	}

    public function about()
    {
        return view("pages.about");
    }

    public function product($product_name)
    {
        $products = array(
            "super_food" => array(
                "img" => asset('img/product/swole_protein.jpg'),
                "header" => "Super Food",
                "content" => "A blend of the best quality whey protein, rolled oats, flax seeds, whole nuts, 
                              cranberries, blueberries with zero added sugar. A superfood for best results 
                              both pre workout, post workout or between meals.",
                "price_tag" => "Price: 100 &#x20b9; a bar",                              
                "ingredient_item" => array("Peanut butter", "Whey protein isolate", "Mixed berries", "Flax seeds", "Chia seeds","Sesame seeds","Melon seeds","Dark Chocolate chips","Dates"),
            ),

            "super_seedy_bar" => array(
                "img" => asset('img/product/super_seedy_bar.jpg'),
                "header" => "Super Seedy",
                "content" => "The ultimate breakfast bar with raw flax seeds, chia seeds, melon seeds,
                             dark chocolate chips and oats. The best way to start your day.",
                "price_tag" => "Price: 100 &#x20b9; a bar",                             
                "ingredient_item" => array("Rolled oats", "Mixed berries", "Flax seeds", "Chia seeds","Sesame seeds","Melon seeds","Dark Chocolate chips","Dates"), 
            ),

            "protein_granola" => array(
                "img" => asset('img/product/protein_trail_mix.jpg'),
                "header" => "Protein 'Granola'",
                "content" => "Protein Cereal? Yes please ! Mix it with milk or snack on it raw.",
                "price_tag" => "Price: INR &#x20b9; a bar",
                "ingredient_item" => array("Flax seeds", "Chia seeds","Sesame seeds","Melon seeds","Dark Chocolate chips","Dates"),
            ),

            "dark_chocolate_bark" => array(
                "img" => asset('img/product/dark_chocolate_bark.jpg'),
                "header" => "Dark Chocolate Bark",
                "content" => "TPS 70 per cent dark chocolate. Satisfy your sugar pangs with the
                             best quality dark chocolate.",
                "price_tag" => "Price: INR &#x20b9; a bar",                             
                "ingredient_item" => array("70 percent cocoa organic chocolate"),             
            ),

            "raw_protein_fudge" => array(
                "img" => asset('img/product/raw_protein_fudge.jpg'),
                "header" => "‘Raw' Protein Fudge",
                "content" => "The best tasting protein dessert. Get 4 grams of protein every time 
                              you decide to cheat on your diet. Is that really cheating?",
                "price_tag" => "Price: INR &#x20b9; a bar",                              
                "ingredient_item" => array("Flax seeds", "Chia seeds","Sesame seeds","Melon seeds","Dark Chocolate chips","Rolled oats","Peanut butter","Whey protein"),              
                
            ),

            "chai_bars" => array(
                "img" => asset('img/product/raw_protein_fudge.jpg'),
                "header" => "Chai Bars",
                "content" => "For those who love their tea, we made their dream come true..",
                "price_tag" => "Price: 80 &#x20b9; a bar",
                "ingredient_item" => array("Chai concentrate", "Rolled oats","Berries","Honey","Dates"),              
                
            ),
        );

        return view("pages.product")->with(['product'=>$products[$product_name], ]);
    }

    public function order()
    {
        return view("pages.order");
    }

    public function orderPost(Requests\OrderRequest $orderRequest)
    {
        $data = [
                    'name' => $orderRequest['name'],
                    'email' => $orderRequest['email'],
                    'tel' => $orderRequest['tel']
                ];

        if(!empty($orderRequest['message'])){
            $data['message']  = $orderRequest['message'];
        }

        $sendToEmail = 'saurabh.pralhad.ghule@gmail.com'; // Default send-to email address of THE PROTEIN STATION

        Mail::send('emails.contact-details', ['data' => $data], function ($message) use ($sendToEmail, $data) {
            $message->to($sendToEmail)->subject('New Enquiry : Contact Page');
        });

        return response()->json(['status' => 'success'],200);        
    }
}
