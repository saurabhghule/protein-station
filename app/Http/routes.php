<?php

Route::get('/', ['as' => 'home', 'uses' => 'WebsiteController@index']);
Route::get('/about', ['as' => 'about', 'uses' => 'WebsiteController@about']);
Route::get('/product/{product_name}', ['as' => 'product', 'uses' => 'WebsiteController@product']);
Route::get('/order', ['as' => 'order', 'uses' => 'WebsiteController@order']);

Route::post('order/post', ['uses' => 'WebsiteController@orderPost', 'as' => 'order.post', ]);




