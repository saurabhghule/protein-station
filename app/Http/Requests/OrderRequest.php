<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class OrderRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'name' => 'required',
			'email' => 'required|email',
			'tel' => 'required'
		];
	}

	 public function messages()
    {
        return [
            'name.required' => 'Please provide Your Name',
            'email.email' => 'Please provide valid Email Id',
            'tel.required' => 'Please provide valid Contact Number',
        ];
    }

}
