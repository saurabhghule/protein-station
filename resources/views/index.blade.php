<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <meta name="_route" content="{{ Route::currentRouteName() }}"/>
	<title>Protein Station</title>

	<link href="{{ asset('css/app.css') }}" rel="stylesheet">

	<!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Ubuntu:400,500' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>

    <style>
        .location-map{
            width: 100%;
            height: 100%;
            margin-top: 50px;
        }
        .location-map #map {
            height:500px;
        }
    </style>

</head>
<body>

    @include('pages.partials.header')

	@yield('content')

    @include('pages.partials.footer')

	<!-- Scripts -->
    <script src="{{ asset('js/all.js') }}"></script>

    <script>
        $(document).ready(
            function() {
                $("html body").niceScroll({
                    scrollspeed:60,
                    mousescrollstep:50,
                    cursorwidth:7,
                    cursorborder:'transparent'
                });
            }
        );
    </script>
</body>
</html>
