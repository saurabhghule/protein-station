<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<table style="width:100%;max-width:500px;">
		<tbody>
			<tr>
				<td style="color:#000000;font-weight:600;width:50%;">Name :</td>
				<td style="color:#000000;width:50%;">{{$data['name']}}</td>
			</tr>
			
			<tr>
				<td style="color:#000000;font-weight:600;width:50%;">Email Address :</td>
				<td style="color:#000000;width:50%;">{{$data['email']}}</td>
			</tr>
			
			<tr>
				<td style="color:#000000;font-weight:600;width:50%;">Phone Number :</td>
				<td style="color:#000000;width:50%;">{{$data['tel']}}</td>
			</tr>

			@if(isset($data['message']))
				<tr>
					<td style="color:#000000;font-weight:600;width:50%;">Message :</td>
					<td style="color:#000000;width:50%;">{{$data['message']}}</td>
				</tr>				
			@endif
		</tbody>
	</table>
</body>
</html>