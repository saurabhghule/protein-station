@extends('index')

@section('content')

    <section class="head-banner-section">
        <div class="banner-bg"></div>
    </section>

    <section class="section-1 aboutSection bgColor-yellow about-sec-padding">

        <div class="slant-bg bgColor-yellow"></div>

        <div class="row">
            <div class="columns small-12 medium-12 large-12">
                <div class="section-header">
                    <h2 class="whiteColor cursive-font no-margin fadeInDown-animation">
                        A few words about us
                    </h2>
                </div>
            </div>
        </div>
        
        <div class="row valign">
            <div class="columns small-12 large-5">
                <div class="aboutCard">
                    <img class="rotateInUpLeft-animation" src="{{ asset('/img/about/pic-1.jpg') }}" alt=""/>
                </div>
            </div>
            <div class="columns small-12 large-7">
                <div class="aboutCard-content">
                    <h3 class="whiteColor small-only-margin25 fadeInUpBig-animation1">
                        We aspire to make healthy eating tasty.
                    </h3>

                    <p class="whiteColor fadeInUpBig-animation2 fontSize1">
                        80% of diets world over lack the nutrition they need. In a world of hustle and little time to focus on 
                        nutrition, The Protein Station comes to the rescue .
                    </p>

                    <p class="whiteColor fadeInUpBig-animation3 fontSize1">
                        We carefully curate and handpick the best quality ingredients for you. Working closely with leading 
                        nutritionists and dieticians, we design products that will help you eat healthy and make you come back for more.
                    </p>

                    <p class="whiteColor fadeInUpBig-animation4 fontSize1">
                        We make sure to fill the gaps missed out on by the big corporate goliaths. With an ever growing portfolio
                        of exciting products, The Protein Station will help you (ch)eat better.
                    </p>

                    {{--<a href="" class="read-more-btn whiteColor hvr-sweep-to-top">READ MORE</a>--}}
                </div>
            </div>
        </div>
    </section>

    <section class="section-2 teamSection team-animation bgColor-black team-sec-padding">

        <div class="slant-bg bgColor-black"></div>

        <div class="row">
            <div class="columns large-12 small-12 medium-12">
                <div class="section-header fadeInDown-animation team-animation-0">
                    <h2 class="whiteColor cursive-font no-margin">
                        Meet Our Team
                    </h2>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="columns large-3 small-12 medium-3">
                <div class="team-card flipInY-animation1 team-animation-1">
                    <div class="founder-img">
                        <!-- <img src="{{ asset('/img/about/team/founder1.jpg') }}" alt=""/> -->
                        <i class="fa fa-male text-center"></i>
                        <div class="img-overlay"></div>
                    </div>
                    <div class="founder-content">
                        <h4 class="whiteColor custom-margin">Krish Belani</h4>
                        <p class="whiteColor">
                            Krish has been a gym junkie for the past five years. After struggling with weight, he decided to change his
                            lifestyle one summer and has never looked back.
                        </p>
                    </div>
                    <div class="social-icons"></div>
                </div>
            </div>
            <div class="columns large-3 small-12 medium-3">
                <div class="team-card flipInY-animation2 team-animation-2">
                    <div class="founder-img">
                        <i class="fa fa-female text-center"></i>
                        <!-- <img src="{{ asset('/img/about/team/founder3.jpg') }}" alt=""/> -->
                        <div class="img-overlay"></div>
                    </div>
                    <div class="founder-content">
                        <h4 class="whiteColor custom-margin">Sakshi Belani</h4>
                        <p class="whiteColor">
                            Realising her passion for health food, she has conceptualised most of the recipes for TPS.
                        </p>
                    </div>
                    <div class="social-icons"></div>
                </div>
            </div>
            <div class="columns large-3 small-12 medium-3">
                <div class="team-card flipInY-animation3 team-animation-3">
                    <div class="founder-img">
                        <i class="fa fa-female text-center"></i>
                        <!-- <img src="{{ asset('/img/about/team/founder3.jpg') }}" alt=""/> -->
                        <div class="img-overlay"></div>
                    </div>
                    <div class="founder-content">
                        <h4 class="whiteColor custom-margin">Aakriti Bahl</h4>
                        <p class="whiteColor">
                            A trained dancer, she extends her creativity to her marketing initiatives for TPS. Touch base with us on 
                            our social media platforms to know more.
                        </p>
                    </div>
                    <div class="social-icons"></div>
                </div>
            </div>

            <div class="columns large-3 small-12 medium-3">
                <div class="team-card flipInY-animation4 team-animation-4">
                    <div class="founder-img">
                        <i class="fa fa-male text-center"></i>
                        <!-- <img src="{{ asset('/img/about/team/founder1.jpg') }}" alt=""/> -->
                        <div class="img-overlay"></div>
                    </div>
                    <div class="founder-content">
                        <h4 class="whiteColor custom-margin">Vishwa Naik</h4>
                        <p class="whiteColor">
                            Having played competitive sports such as Tennis and Basketball at the state and district level, Vishwa 
                            learned the importance of nutrition early on.
                        </p>
                    </div>
                    <div class="social-icons"></div>
                </div>
            </div>
        </div>

    </section>

@endsection