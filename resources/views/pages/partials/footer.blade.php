
<footer class="padding50 footer-section medium-only-text-center small-only-text-center">

    <div class="slant-bg-1"></div>

    <div class="row">
        <div class="columns large-4">
            <div class="logo text-center">
                <img src="{{ asset('/img/logo.png') }}" alt="Protein Station"/>
            </div>

            <h6 class="note small-only-font text-center">
                © The Protein Station - All Rights Reserved
            </h6>
        </div>

        <div class="columns large-4">
            <div class="contactDetails">
                <h5 class="blackColor">
                    5 Sindh Chambers,<br>
                    Shahid Bhagat Singh Road,<br>
                    Colaba, Mumbai - 400005.
                </h5>
                <h5 class="blackColor">Phone : 9920954954 / 9821854954</h5>

                <h5>
                    <a class="blackColor" href="">Email : orders@theproteinstation.com</a>
                </h5>
            </div>
        </div>
        <div class="columns large-4">
            <div class="social-icons">

                <h5 class="blackColor">
                    Hit us up on any social media platform and join in on this incredible quest for the best!
                </h5>

                <ul class="small-only-margin25">
                    <li class="left">
                        <a href="https://www.facebook.com/theproteinstation" target="_blank">
                            <i class="fa fa-facebook-square"></i>
                        </a>
                    </li>
                    <li class="left">
                        <a href="https://twitter.com/proteinstation1" target="_blank">
                            <i class="fa fa-twitter-square"></i>
                        </a>
                    </li>
                    <li class="left">
                        <a href="https://www.instagram.com/theproteinstation" target="_blank">
                            <i class="fa fa-instagram"></i>
                        </a>
                    </li>
                </ul>

                <h6>
                    Powered by <a class="blackColor" href="https://www.speakinglamp.com">Speaking<span class="">Lamp</span></a>
                </h6>
            </div>
        </div>
    </div>
</footer>