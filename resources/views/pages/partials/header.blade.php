

<header class="proteinStation-header hide-for-small-only">
    <div class="header-wrapper">
        <div class="row">
            <div class="columns large-12">
                <nav>
                    <div class="left brand-logo">
                        <a href="{{route('home')}}" class="brand-img">
                            <img src="{{ asset('/img/logo.png')}}" alt="Protein Station"/>
                        </a>
                    </div>

                    <div class="right navigation-bar">
                        <ul class="no-margin nav-menu right">
                            <li class="navMenu-item">
                                <a class="whiteColor navMenu-link" href="{{route('home')}}">HOME</a>
                            </li>
                            <li class="navMenu-item">
                                <a class="whiteColor navMenu-link" href="{{route('about')}}">ABOUT</a>
                            </li>
                            <li id="products" class="navMenu-item">
                                <a class="whiteColor navMenu-link">PRODUCTS</a>
                                <ul class="productMenu">
                                    <li class="bgColor-black text-center productMenu-item"><a class="whiteColor productMenu-link" href="{{route('product', 'super_food')}}">Super Food</a></li>
                                    <li class="bgColor-black text-center productMenu-item"><a class="whiteColor productMenu-link" href="{{route('product', 'super_seedy_bar')}}">Super Seedy</a></li>
                                    <li class="bgColor-black text-center productMenu-item"><a class="whiteColor productMenu-link" href="{{route('product', 'protein_granola')}}">Protein 'Granola'
                                    </a></li>
                                    <li class="bgColor-black text-center productMenu-item"><a class="whiteColor productMenu-link" href="{{route('product', 'dark_chocolate_bark')}}">Dark Chocolate Bark
                                    </a></li>
                                    <li class="bgColor-black text-center productMenu-item"><a class="whiteColor productMenu-link" href="{{route('product', 'raw_protein_fudge')}}">Raw Protein Fudge</a></li>
                                    <li class="bgColor-black text-center productMenu-item"><a class="whiteColor productMenu-link" href="{{route('product', 'chai_bars')}}">Chai Bars</a></li>
                                </ul>
                            </li>
                            <li class="navMenu-item">
                                <a class="whiteColor navMenu-link" href="https://theproteinstation.wordpress.com/" target="_blank">BLOGS</a>
                            </li>
                            <li class="navMenu-item">
                                <a class="whiteColor navMenu-link" href="{{route('order')}}">ORDER</a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</header>


<div class="mobile-header hide-for-large-up hide-for-medium-only">
    <nav class="top-bar" data-topbar role="navigation">
        <ul class="title-area">
            <li class="name text-center">
                <a href="{{route('home')}}" class="brand-img">
                    <img src="{{ asset('/img/logo.png')}}" alt="Protein Station"/>
                </a>
            </li>

            <li class="toggle-topbar menu-icon left"><a href="#"><span></span></a></li>
        </ul>

        <section class="top-bar-section">
            <ul class="right menu">
                <li class="menu-items mh-border-top"><a class="text-center mob-header-font" href="{{route('home')}}">HOME</a></li>
                <li class="menu-items mh-border-top"><a class="text-center mob-header-font" href="{{route('about')}}">ABOUT</a></li>
                <li class="has-dropdown menu-items mh-border-top">
                    <a class="text-center mob-header-font">PRODUCTS</a>
                    <ul class="dropdown">
                        <li class="text-center mh-border-top"><a class="whiteColor mob-header-font" href="{{route('product','super_food')}}">Super Food</a></li>
                        <li class="text-center mh-border-top"><a class="whiteColor mob-header-font" href="{{route('product','super_seedy_bar')}}">Super Seedy</a></li>
                        <li class="text-center mh-border-top"><a class="whiteColor mob-header-font" href="{{route('product','protein_granola')}}">Protein 'Granola'</a></li>
                        <li class="text-center mh-border-top"><a class="whiteColor mob-header-font" href="{{route('product','dark_chocolate_bark')}}">Dark Chocolate Bark</a></li>
                        <li class="text-center mh-border-top"><a class="whiteColor mob-header-font" href="{{route('product','raw_protein_fudge')}}">Raw Protein Fudge</a></li>
                        <li class="text-center mh-border-top"><a class="whiteColor mob-header-font" href="{{route('product','chai_bars')}}">Chai Bars</a></li>
                    </ul>
                </li>
                <li class="menu-items mh-border-top"><a class="text-center mob-header-font" href="https://theproteinstation.wordpress.com/">BLOG</a></li>
                <li class="menu-items mh-border-top"><a class="text-center mob-header-font" href="{{route('order')}}">CONTACT</a></li>
            </ul>
        </section>
    </nav>
</div>











