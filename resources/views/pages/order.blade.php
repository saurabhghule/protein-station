@extends('index')

@section('content')
    
    <script src="https://maps.googleapis.com/maps/api/js"></script>
    <script>
        function initialize() {
            var mapCanvas = document.getElementById('map');
            var myLatLng = new google.maps.LatLng(18.918539, 72.829050);
            var mapOptions = {
                center: myLatLng,
                zoom: 18,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                scrollwheel: false
            };
            var map = new google.maps.Map(mapCanvas, mapOptions)
            var marker = new google.maps.Marker({
                position: myLatLng,
                map: map,
                title: 'Protein Station'
            });
        }
        google.maps.event.addDomListener(window, 'load', initialize);
    </script>


    <section class="head-banner-section">
        <div class="banner-bg"></div>
    </section>

    <section class="section-1 contactDetails-section bgColor-yellow contact-sec-padding">

        <div class="slant-bg bgColor-yellow"></div>

        <div class="row">
            <div class="columns small-12 large-4">
                <div class="section-header fadeInDown-animation1">
                    <h2 class="whiteColor cursive-font no-margin">
                        Objectives
                    </h2>
                </div>

                <div class="contactAddress fadeInRightBig-animation1">
                    <h5 class="whiteColor">
                        <!-- For your snack cravings we offer our no sugar bars with 23g protein and our no sugar flax seed nutrition bars. -->
                        There's no need to feel guilty when you indulge anymore .
                    </h5>

                    <h5 class="whiteColor">
                        <!-- For the worst cravings, those chocolate ones that make you finish a box of truffles we offer:
                        no sugar chocolate truffles. -->
                        A healthy lifestyle is now just a click away.
                    </h5>

                    <h5 class="whiteColor">
                        So spare your guilt through those endless excuses and contact us today!
                    </h5>
                </div>
            </div>
            <div class="columns small-12 large-8">
                <div class="section-header fadeInDown-animation2">
                    <h2 class="whiteColor cursive-font no-margin">
                        Contact Form
                    </h2>
                </div>
                <div class="contactForm fadeInRightBig-animation2">
                    <form action="{{ route('order.post') }}" class="contact-form" method="POST">
                        <input type="hidden" value="{{ csrf_token() }}" name="_token"/>
                        <input type="hidden" value="contact-page-form" name="form_id"/>

                        <div class="row">
                            <div class="columns small-12 large-4">
                                <input name="name" type="text" placeholder="Name"/>
                                <!-- <small class="error">Name is required and must be a string.</small> -->
                            </div>
                            <div class="columns small-12 large-4">
                                <input name="email" type="email" placeholder="Email"/>
                                <!-- <small class="error">An email address is required.</small> -->
                            </div>
                            <div class="columns small-12 large-4">
                                <input name="tel" type="tel" placeholder="Phone"/>
                            </div>
                            <div class="columns small-12 large-12">
                                <textarea name="message" placeholder="Message"></textarea>
                            </div>

                            <div class="columns small-6 large-3">
                                <button type="reset" class="clear-btn whiteColor text-center hvr-sweep-to-top no-margin">CLEAR</button>
                            </div>
                            <div class="columns small-6 large-3 end">
                                <button class="send-btn whiteColor text-center hvr-sweep-to-top no-margin">SEND</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- <div class="row">
            <div class="columns large-12 small-12 medium-12">
                <div class="location-map">
                    <div id="map"></div>
                </div>
            </div>
        </div> -->
    </section>

@endsection