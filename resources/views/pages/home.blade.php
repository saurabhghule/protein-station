@extends('index')

@section('content')

    <section class="home-main-banner">
        <div class="bannerSlider">
            <div class="slide" id="slide-1">
                <div class="banner-img"></div>
                <div class="overlay"></div>
            </div>
            <div class="slide" id="slide-2">
                <div class="banner-img"></div>
                <div class="overlay"></div>
            </div>
            <div class="slide" id="slide-3">
                <div class="banner-img"></div>
                <div class="overlay"></div>
            </div>
            <div class="slide" id="slide-4">
                <div class="banner-img"></div>
                <div class="overlay"></div>
            </div>
        </div>
    </section>

    <section class="section-1 home-product-section home-product-animation bgColor-yellow home-sec1-padding">
        <div class="slant-bg bgColor-yellow"></div>

        <div class="row">
            <div class="columns small-12 medium-12 large-12">
                <div class="section-header fadeInDown-animation">
                    <h2 class="whiteColor cursive-font no-margin">
                        Our Products
                    </h2>
                </div>
            </div>
        </div>

        <div class="row read-more-container">
            <div class="columns large-4 medium-4 small-12">
                <div class="product-card fadeInUp-animation1">
                    <a id="img-1" class="product-img">
                        <img src="{{ asset('img/home/products/p1.jpg') }}" alt=""/>
                        <div class="img-overlay"></div>
                    </a>
                    <p class="price-tag whiteColor">Price: 100 &#x20b9; a bar</p>
                    <a href="{{route('product', 'super_seedy_bar')}}" class="read-more-btn whiteColor hvr-sweep-to-top">READ MORE</a>
                </div>
            </div>
            <div class="columns large-4 medium-4 small-12">
                <div class="product-card fadeInUp-animation2">
                    <a id="img-2" class="product-img">
                        <img src="{{ asset('/img/home/products/p2.jpg')}}" alt=""/>
                        <div class="img-overlay"></div>
                    </a>
                    <p class="price-tag whiteColor">Price: 100 &#x20b9; a bar</p>
                    <a href="{{route('product', 'super_food')}}" class="read-more-btn whiteColor hvr-sweep-to-top">READ MORE</a>
                </div>
            </div>
            <div class="columns large-4 medium-4 small-12">
                <div class="product-card fadeInUp-animation3">
                    <a id="img-1" class="product-img">
                        <img src="{{ asset('/img/home/products/p3.jpg')}}" alt=""/>
                        <div class="img-overlay"></div>
                    </a>
                    <p class="price-tag whiteColor">Price: INR &#x20b9; a bar</p>
                    <a href="{{route('product', 'raw_protein_fudge')}}" class="read-more-btn whiteColor hvr-sweep-to-top">READ MORE</a>
                </div>
            </div>

            <div class="columns large-4 medium-4 small-12">
                <div class="product-card fadeInUp-animation1">
                    <a id="img-1" class="product-img">
                        <img src="{{ asset('img/home/products/p4.jpg') }}" alt=""/>
                        <div class="img-overlay"></div>
                    </a>
                    <p class="price-tag whiteColor">Price: INR &#x20b9; a bar</p>
                    <a href="{{route('product', 'dark_chocolate_bark')}}" class="read-more-btn whiteColor hvr-sweep-to-top">READ MORE</a>
                </div>
            </div>
            <div class="columns large-4 medium-4 small-12">
                <div class="product-card fadeInUp-animation2">
                    <a id="img-2" class="product-img">
                        <img src="{{ asset('/img/home/products/p5.jpg')}}" alt=""/>
                        <div class="img-overlay"></div>
                    </a>
                    <p class="price-tag whiteColor">Price: INR &#x20b9; a bar</p>
                    <a href="{{route('product', 'protein_granola')}}" class="read-more-btn whiteColor hvr-sweep-to-top">READ MORE</a>
                </div>
            </div>
            <div class="columns large-4 medium-4 small-12">
                <div class="product-card fadeInUp-animation3">
                    <a id="img-1" class="product-img">
                        <img src="{{ asset('/img/home/products/p3.jpg')}}" alt=""/>
                        <div class="img-overlay"></div>
                    </a>
                    <p class="price-tag whiteColor">Price: 80 &#x20b9; a bar</p>
                    <a href="{{route('product', 'chai_bars')}}" class="read-more-btn whiteColor hvr-sweep-to-top">READ MORE</a>
                </div>
            </div>
        </div>
    </section>

    <section class="section-2 home-about-section home-about-animation bgColor-black home-sec2-padding">
        <div class="slant-bg-1 bgColor-black"></div>

        <div class="row">
            <div class="columns small-12 medium-12 large-12">
                <div class="section-header fadeInDown-animation">
                    <h2 class="whiteColor cursive-font no-margin">
                        About Us
                    </h2>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="columns small-12 medium-12 large-12">
                <div class="about-us-content bounceInRight-animation">
                    <p class="whiteColor">
                        We are a health foods company, that was born with one goal in mind - to help you (ch)eat better !
                    </p>

                    <p class="whiteColor">
                        Working with food technologists we aspire to serve you products that are not only healthy for you but also
                        delicious in taste.
                    </p>

                    <p class="whiteColor">
                       So whether you are stuck at work, pulling an all nighter studying or getting ready to work out - TPS has 
                        you covered !
                    </p>
                </div>
            </div>
        </div>

        <div class="slant-bg-2 bgColor-black small-only-height70"></div>
    </section>

    <section class="section-3 home-gallery-section home-gallery-animation">
        <div class="bg-img home-sec3-padding">
            <div class="row">
                <div class="columns small-12 medium-12 large-12">
                    <div class="section-header fadeInDown-animation">
                        <h2 class="whiteColor cursive-font no-margin">
                            Our Gallery
                        </h2>
                    </div>
                </div>
            </div>

            <div class="productGrid-wrapper tabs-content">
                <div id="panel1" class="product-grid content active">
                    <div class="row">
                        <div class="columns small-12 large-4 medium-12">
                            <a class="product-img med-only-margin-bottom20 fadeIn-animation1" href="">
                                <img src="{{ asset('/img/home/gallery/g1.jpg') }}" alt=""/>
                                <div class="product-overlay">
                                    <div class="product-desc">
                                        <h5 class="whiteColor">PRODUCT 1</h5>
                                        <p class="whiteColor">
                                            Lorem ipsum dolor sit amet,
                                            consectetur adipisicing elit.
                                            Animi beatae commodi, doloremque,
                                        </p>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="columns small-12 large-4 medium-12">
                            <a class="product-img med-only-margin-bottom20 fadeIn-animation2" href="">
                                <img src="{{ asset('/img/home/gallery/g2.jpg') }}" alt=""/>
                                <div class="product-overlay">
                                    <div class="product-desc">
                                        <h5 class="whiteColor">PRODUCT 1</h5>
                                        <p class="whiteColor">
                                            Lorem ipsum dolor sit amet,
                                            consectetur adipisicing elit.
                                            Animi beatae commodi, doloremque,
                                        </p>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="columns small-12 large-4 medium-12">
                            <a class="product-img med-only-margin-bottom20 fadeIn-animation3" href="">
                                <img src="{{ asset('/img/home/gallery/g3.jpg') }}" alt=""/>
                                <div class="product-overlay">
                                    <div class="product-desc">
                                        <h5 class="whiteColor">PRODUCT 1</h5>
                                        <p class="whiteColor">
                                            Lorem ipsum dolor sit amet,
                                            consectetur adipisicing elit.
                                            Animi beatae commodi, doloremque,
                                        </p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="columns small-12 large-4 medium-12">
                            <a class="product-img med-only-margin-bottom20 fadeIn-animation4 large-only-margin25 large-up-margin-t-25" href="">
                                <img src="{{ asset('/img/home/gallery/g4.jpg') }}" alt=""/>
                                <div class="product-overlay">
                                    <div class="product-desc">
                                        <h5 class="whiteColor">PRODUCT 1</h5>
                                        <p class="whiteColor">
                                            Lorem ipsum dolor sit amet,
                                            consectetur adipisicing elit.
                                            Animi beatae commodi, doloremque,
                                        </p>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="columns small-12 large-4 medium-12">
                            <a class="product-img med-only-margin-bottom20 fadeIn-animation5 large-only-margin25 large-up-margin-t-25" href="">
                                <img src="{{ asset('/img/home/gallery/g5.jpg') }}" alt=""/>
                                <div class="product-overlay">
                                    <div class="product-desc">
                                        <h5 class="whiteColor">PRODUCT 1</h5>
                                        <p class="whiteColor">
                                            Lorem ipsum dolor sit amet,
                                            consectetur adipisicing elit.
                                            Animi beatae commodi, doloremque,
                                        </p>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="columns small-12 large-4 medium-12">
                            <a class="product-img med-only-margin-bottom20 fadeIn-animation6 large-only-margin25 large-up-margin-t-25" href="">
                                <img src="{{ asset('/img/home/gallery/g6.jpg') }}" alt=""/>
                                <div class="product-overlay">
                                    <div class="product-desc">
                                        <h5 class="whiteColor">PRODUCT 1</h5>
                                        <p class="whiteColor">
                                            Lorem ipsum dolor sit amet,
                                            consectetur adipisicing elit.
                                            Animi beatae commodi, doloremque,
                                        </p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection