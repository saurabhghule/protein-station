@extends('index')

@section('content')

    <section class="head-banner-section">
        <div class="banner-bg"></div>
    </section>

    <section class="section-1 productSection bgColor-yellow product-sec-padding">
        <div class="slant-bg bgColor-yellow"></div>

        <div class="row">
            <div class="columns large-8 small-12 medium-12">
                <div class="product-img fadeIn-animation">
                    <img src="{{ $product['img'] }}" alt=""/>
                </div>

                <div class="product-header fadeInDown-animation">
                    <h4 class="whiteColor">
                        {{ $product['header'] }}
                    </h4>
                </div>

                <div class="product-desc bounceInRight-animation">
                    <p class="whiteColor">
                        {{ $product['content'] }}
                    </p>
                </div>

                <div class="product-price bounceInRight-animation">
                    <p class="whiteColor">
                        {{ $product['price_tag'] }}
                    </p>
                </div>
            </div>

            <div class="columns large-4 small-12 medium-12">
                <div class="section-header bounceInRight-animation">
                    <h2 class="whiteColor cursive-font no-margin">
                        Ingredients
                    </h2>
                </div>

                <div class="categoryList">
                    <ul class="menu no-margin">
                        @foreach($product['ingredient_item'] as $item)
                            <li class="menu-items fadeInDown-animation">
                                <div href="" class="whiteColor">
                                    <i class="fa fa-arrow-circle-right"></i>
                                    <span>{{ $item }}</span>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="columns large-8 small-12 medium-12">
                <div class="enquirySection margin-top20">
                    <div class="section-header">
                        <h2 class="whiteColor cursive-font no-margin">
                            Order Now
                        </h2>
                    </div>

                    <div class="contactForm">
                        <form data-abide>
                            <div class="row">
                                <div class="columns small-12 large-6">
                                    <input type="text" placeholder="Name" required pattern="[a-zA-Z]+"/>
                                    <small class="error">Name is required and must be a string.</small>
                                </div>
                                <div class="columns small-12 large-6">
                                    <input type="email" placeholder="Email" required/>
                                    <small class="error">An email address is required.</small>
                                </div>

                                <div class="columns small-12 large-12">
                                    <textarea placeholder="Message"></textarea>
                                </div>

                                <div class="columns small-6 large-3 end">
                                    <button type="submit" class="send-btn whiteColor text-center hvr-sweep-to-top no-margin">SUBMIT</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection