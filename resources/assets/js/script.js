
var _route = '';

$(document).ready(function(){

    _route = $('meta[name=_route]').attr('content');

    $(document).foundation();

    /* HEADER ANIMATION */
    if($('.proteinStation-header').is(":visible")){
        $(window).on("scroll",function(){
            if($(window).scrollTop() > 50){
                $(".proteinStation-header").find(".header-wrapper").addClass("stickyHeader");
            }
            else{
                $(".proteinStation-header").find(".header-wrapper").removeClass("stickyHeader");
            }
        })
    }

    /* HOME PAGE */
    if(_route == 'home'){
        $('.bannerSlider').slick({
            arrows: true,
            autoplay: true,
            pauseOnHover: false,
            autoplaySpeed: 5000,
            speed: 300,
            cssEase: 'ease',
            fade:true,
            infinite: true,
            lazyLoad: "ondemand",
            prevArrow: '<span class="nav-prev"><i class="fa fa-angle-left"></i></span>',
            nextArrow: '<span class="nav-next"><i class="fa fa-angle-right"></i></span>'
        });

        $(window).on("scroll", function () {

            if ($(".home-product-section").visible(true)) {

                var scrollAnimationElements = 4;
                var animationsCompleted = 0;

                 setTimeout(function () {
                 $(".home-product-section").find(".fadeInDown-animation").addClass("fadeInDown");
                 }, 500);
                 animationsCompleted++;


                 setTimeout(function () {
                 $(".home-product-section").find(".fadeInUp-animation1").addClass("fadeInUp");
                 }, 1000);
                 animationsCompleted++;


                 setTimeout(function () {
                 $(".home-product-section").find(".fadeInUp-animation2").addClass("fadeInUp");
                 }, 1500);
                 animationsCompleted++;


                 setTimeout(function () {
                 $(".home-product-section").find(".fadeInUp-animation3").addClass("fadeInUp");
                 }, 2000);
                 animationsCompleted++;

                 if (animationsCompleted == scrollAnimationElements) {
                 $(".home-product-section").removeClass("home-product-animation");
                 }
            }

            if ($(".home-about-section").visible(true)) {
                var scrollAnimationElements = 2;
                var animationsCompleted = 0;

                 setTimeout(function () {
                 $(".home-about-section").find(".fadeInDown-animation").addClass("fadeInDown");
                 }, 500);
                 animationsCompleted++;

                 setTimeout(function () {
                 $(".home-about-section").find(".bounceInRight-animation").addClass("bounceInRight");
                 }, 1000);
                 animationsCompleted++;

                 if (animationsCompleted == scrollAnimationElements) {
                 $(".home-about-section").removeClass("home-about-animation");
                 }
            }

            if($(".home-gallery-section").visible(true)){
                var scrollAnimationElements = 7;
                var animationsCompleted = 0;

                setTimeout(function () {
                    $(".home-gallery-section").find(".fadeInDown-animation").addClass("fadeInDown");
                }, 500);
                animationsCompleted++;

                setTimeout(function () {
                    $(".home-gallery-section").find(".fadeIn-animation1").addClass("fadeIn");
                }, 1000);
                animationsCompleted++;

                setTimeout(function () {
                    $(".home-gallery-section").find(".fadeIn-animation2").addClass("fadeIn");
                }, 1500);
                animationsCompleted++;

                setTimeout(function () {
                    $(".home-gallery-section").find(".fadeIn-animation3").addClass("fadeIn");
                }, 2000);
                animationsCompleted++;

                setTimeout(function () {
                    $(".home-gallery-section").find(".fadeIn-animation4").addClass("fadeIn");
                }, 2500);
                animationsCompleted++;

                setTimeout(function () {
                    $(".home-gallery-section").find(".fadeIn-animation5").addClass("fadeIn");
                }, 3000);
                animationsCompleted++;

                setTimeout(function () {
                    $(".home-gallery-section").find(".fadeIn-animation6").addClass("fadeIn");
                }, 3500);
                animationsCompleted++;

                if (animationsCompleted == scrollAnimationElements) {
                    $(".home-gallery-section").removeClass("home-gallery-animation");
                }
            }
        });
    }


    /* ABOUT PAGE */
    if(_route == 'about'){

        if($(".aboutSection").visible(true)){
            setTimeout(function () {
                $(".aboutSection").find(".fadeInDown-animation").addClass("fadeInDown");
            }, 500);

            setTimeout(function () {
                $(".aboutSection").find(".rotateInUpLeft-animation").addClass("rotateInUpLeft");
            }, 1000);

            setTimeout(function () {
                $(".aboutSection").find(".fadeInUpBig-animation1").addClass("fadeInUpBig");
            }, 1000);

            setTimeout(function () {
                $(".aboutSection").find(".fadeInUpBig-animation2").addClass("fadeInUpBig");
            }, 1500);

            setTimeout(function () {
                $(".aboutSection").find(".fadeInUpBig-animation3").addClass("fadeInUpBig");
            }, 2000);
            setTimeout(function () {
                $(".aboutSection").find(".fadeInUpBig-animation4").addClass("fadeInUpBig");
            }, 2500);
        }

        if($(".teamSection").is(":visible")){

            var scrollAnimationElements = 4;
            var animationsCompleted = 0;

            $(window).on("scroll", function () {

                setTimeout(function () {
                    $(".teamSection").find(".fadeInDown-animation").addClass("fadeInDown");
                }, 500);
                animationsCompleted++;

                setTimeout(function () {
                    $(".teamSection").find(".flipInY-animation1").addClass("flipInY");
                }, 1000);
                animationsCompleted++;

                setTimeout(function () {
                    $(".teamSection").find(".flipInY-animation2").addClass("flipInY");
                }, 1500);
                animationsCompleted++;

                setTimeout(function () {
                    $(".teamSection").find(".flipInY-animation3").addClass("flipInY");
                }, 2000);
                animationsCompleted++;

                setTimeout(function () {
                    $(".teamSection").find(".flipInY-animation4").addClass("flipInY");
                }, 2500);
                animationsCompleted++;

                if (animationsCompleted == scrollAnimationElements) {
                    $(".teamSection").removeClass("team-animation");
                }
            })
        }
    }

    /* PRODUCT PAGE */
    if(_route == 'product'){
        if($(".productSection").visible(true)){
            setTimeout(function () {
                    $(".productSection").find(".fadeIn-animation").addClass("fadeIn");
                }, 500);

            setTimeout(function () {
                $(".productSection").find(".bounceInRight-animation").addClass("bounceInRight");
            }, 1000);

            setTimeout(function () {
                $(".productSection").find(".fadeInDown-animation").addClass("fadeInDown");
            }, 1500);

            setTimeout(function () {
                $(".productSection").find(".fadeInDown-animation1").addClass("fadeInDown");
            }, 2000);

            setTimeout(function () {
                $(".productSection").find(".fadeInDown-animation2").addClass("fadeInDown");
            }, 2500);

            setTimeout(function () {
                $(".productSection").find(".fadeInDown-animation3").addClass("fadeInDown");
            }, 3000);

            setTimeout(function () {
                $(".productSection").find(".fadeInDown-animation4").addClass("fadeInDown");
            }, 3500);
        }
    }


    /* CONTACT PAGE */
    if(_route =='order'){
        if($(".contactDetails-section").visible(true)){
            setTimeout(function () {
                $(".contactDetails-section").find(".fadeInDown-animation1").addClass("fadeInDown");
            }, 500);

            setTimeout(function () {
                $(".contactDetails-section").find(".fadeInRightBig-animation1").addClass("fadeInRightBig");
            }, 1000);

            setTimeout(function () {
                $(".contactDetails-section").find(".fadeInDown-animation2").addClass("fadeInDown");
            }, 1500);

            setTimeout(function () {
                $(".contactDetails-section").find(".fadeInRightBig-animation2").addClass("fadeInRightBig");
            }, 2000);
        }
    }

   $('.contact-form').on('submit', function (e) {
        e.preventDefault();
        var form = $(this);
        form.find('button').prop('disabled', true);

        $.ajax({
            url: form.attr('action'),
            method: 'POST',
            data: form.serialize(),
            statusCode: {
                200: function (response) {
                    swal({
                        title: "Got your message!",
                        text: "We will get back to you soon!",
                        type: "success",
                        confirmButtonText: "Cool"
                    });

                    form[0].reset();
                    form.find('button').prop('disabled', false);
                },
                422: function (error) {
                    showFirstError(error);
                    form.find('button').prop('disabled', false);
                }
            }
        });

    });


    //$(".navMenu-item").on({
    //
    //    mouseover: function(){
    //        $(this).off('mouseover');
    //    },
    //    mouseleave: function(){
    //        $(this).off('mouseleave');
    //    }
    //
    //});

    if (Modernizr.mq('only all and (min-width: 992px)')) {

        $('.read-more-section').readmore({
            speed: 75,
            collapsedHeight: $(".read-more-influencer").height() - 70,
            beforeToggle: function (trigger, element, expanded) {
                if (!expanded) {
                    $('[data-readmore-toggle]').each(function () {
                        if ($(this).text() == 'Close') {
                            $(this).click();
                        }
                    });
                }
            },
            afterToggle: function (trigger, element, expanded) {
                if (expanded) {
                    console.log('ayya');
                }
            }
        });

    }


    $(document).click(function(e){
        if($(e.target).is('#products *')){
            console.log("ok we  are in");
            $('.navMenu-item').find('.productMenu').addClass('slideMenu');
        }
        else{
            $('.navMenu-item').find('.productMenu').removeClass('slideMenu');
        }
    });

    $(document).on('mouseover',function (e) {
        if($(e.target).is('#products *')){
            $('.navMenu-item').find('.productMenu').addClass('slideMenu');
        }
        else{
            $('.navMenu-item').find('.productMenu').removeClass('slideMenu');
        }
    });

});


var showFirstError = function (errors) {
    var responseJSON = errors.responseJSON;
    for (var prop in responseJSON) {
        swal({
            title: "Oops!",
            text: responseJSON[prop][0],
            type: "error",
            confirmButtonText: "Okay"
        });
        return;
    }
};






